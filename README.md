# webexcel与webuploader

#### Description
1. 基于流云对easyui的封装基础上拓展的webexcel与webuploader，其中webexcel实现了合并单元格、拆分单元格、字体加粗、背景颜色设定、边框设定、对齐方式、导出excel、插入行、插入列、添加行、添加列、删除行、删除列，拖拽设定行高、列宽、单元格内容编辑等功能，除了基于easyui和流云的拓展，以及颜色选择器使用了开源的之外，其他的webexcel功能，纯个人手敲，其中webuploader部分为基于百度的uploader封装为easyui的方式

#### Software Architecture

1. 使用方法：$(target).webexcel(option);
2. 说明文档见：extend\jquery-easyui-webexcel\doc
3. ![输入图片说明](https://images.gitee.com/uploads/images/2019/0515/124754_25444823_464433.png "1.png")
4. ![输入图片说明](https://images.gitee.com/uploads/images/2019/0515/124826_c606083f_464433.png "2.png")
5. ![输入图片说明](https://images.gitee.com/uploads/images/2019/0515/124842_5f723718_464433.png "3.png")

#### Installation

1. 浏览器直接打开example-excel.html即可看到效果，建议使用谷歌浏览器
2. 谷歌浏览器需设置支持加载本地数据

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)