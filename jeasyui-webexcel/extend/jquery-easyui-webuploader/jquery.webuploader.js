﻿/**
 * jQuery EasyUI 1.3.6
 * Copyright (c) 2009-2014 www.jeasyui.com. All rights reserved.
 *
 * Licensed under the GPL or commercial licenses
 * To use it on other terms please contact author: info@jeasyui.com
 * http://www.gnu.org/licenses/gpl.txt
 * http://www.jeasyui.com/license_commercial.php
 *
 * jQuery EasyUI webuploader Plugin Extensions 1.0 beta
 * jQuery EasyUI webuploader 插件扩展
 * jquery.webuploader.js
 * 二次开发 曹传喜
 * 最近更新：2018-07-18
 *
 * 依赖项：
 *   1、jquery.jdirk.js v1.0 beta late
 *   2、jeasyui.extensions.js v1.0 beta late
 *   3、jeasyui.extensions.menu.js v1.0 beta late
 *   4、jeasyui.extensions.panel.js v1.0 beta late 和 jeasyui.extensions.window.js v1.0 beta late(可选)
 *   5、jeasyui.extensions.progressbar.js v1.0 beta late

 *   6、webuploader/jquery.webuploader.js
 *   7、webuploader/webuploader.css
 *   8、webuploader/webuploader.swf
 *   9、webuploader/webuploader.php|webuploader.ashx|webuploader.jsp
 *
 * Copyright (c) 2013-2014 ChenJianwei personal All rights reserved.
 * http://www.chenjianwei.org
 */
(function ($, undefined) {

    function create(target) {
        var t = $(target).addClass("webuploader-f").hide(), state = $.data(target, "webuploader"), opts = state.options,
            name = t.attr("name");
        if (name) {
            t.attr("webuploaderName", name);
        }
        opts.completefiles = [];
        state.pickerId = "picker_" + $.util.guid("N");
        state.webuploaderId = $.util.guid("N");
        state.uploadBtnId = $.util.guid("N");
        var box = $('<div class="webuploader-box" id="' + state.webuploaderId + '"/>');
        var head = $('<div class="head"/>');
        var body = $('<div class="body"/>');
        head.append('<div id="' + state.pickerId + '">点我选择文件</div>');
        if (!opts.auto) {
            head.append('<a id="' + state.uploadBtnId + '" class="easyui-linkbutton">上传</a>');
        }
        box.append(head).append(body);
        $(target).parent().append(box);
        $.parser.parse('.webuploader-box');
        initWebUploader(target);
        // setValues(target, opts.value);
        // setSize(target);
        // if (opts.disabled) { disable(target, opts.disabled); }
        // setValidation(target)
    }

    function initWebUploader(target) {
        var state = $.data(target, "webuploader"), opts = state.options;
        $.extend(opts, {
            // swf文件路径
            swf: $.util.getRootPath() + '/rsframe/plugins/jeasyui/extend/jquery-easyui-webuploader/webuploader/Uploader.swf',
            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#' + state.pickerId
        });

        opts.uploader = WebUploader.create(opts);
        opts.uploader.on('error', function (type) {
            if ('Q_EXCEED_NUM_LIMIT' == type) {
                $.easyui.messager.show('最多上传' + opts.fileNumLimit + '个文件');
            } else if ('Q_EXCEED_SIZE_LIMIT' == type) {
                $.easyui.messager.show('只能上传小于' + renderSize(opts.fileSizeLimit) + '的文件');
            } else if ('Q_TYPE_DENIED' == type) {
                $.easyui.messager.show('不允许上传的文件类型');
            }
        });
        // 当有文件被添加进队列的时候
        opts.uploader.on('fileQueued', function (file) {
            if ($('#' + state.webuploaderId).find('.webuploader-item').length >= opts.fileNumLimit) {
                opts.uploader.removeFile(file.id, true);
                $.easyui.messager.show('最多上传' + opts.fileNumLimit + '个文件');
                return false;
            } else {
                setItem(target, file);
            }
        });
        // 文件上传过程中创建进度条实时显示。
        opts.uploader.on('uploadProgress', function (file, percentage) {
            $('#' + file.id + '_progressbar').progressbar('setValue', (percentage * 100).toFixed(0));
            $('#' + file.id + '_state').text('正在上传');
            $('#' + file.id + '_retry').hide().removeClass('error');
        });
        opts.uploader.on('uploadSuccess', function (file, json) {
            if (json.success) {
                $('#' + file.id + '_state').text('上传成功');
                $('#' + file.id + '_retry').hide().removeClass('error');
                var fileNames = file.name.split('.');
                var fileName = "";
                for (var j = 0; j < fileNames.length - 1; j++) {
                    fileName += fileNames[j];
                }
                opts.completefiles.push({
                    id: file.id,
                    fullName: file.name,//附件全名，含后缀
                    attName: fileName,//附件名称，不含后缀
                    attSuffix: file.ext,//附件后缀
                    attSize: file.size,//附件大小（单位byte）
                    savePath: json.data.savePath,//相对存储目录
                    mimeType: file.type,//http头中的mimetype字符串
                    hashValue: file._hash,//文件的md5值
                    thumbnailPath: ''//缩略图地址
                });
                setHideInputValue(target);
                //设置超链接
                $('#' + file.id + '_href').attr('href', json.data.savePath);
            }
        });

        opts.uploader.on('uploadError', function (file) {
            $('#' + file.id + '_state').text('上传出错').addClass('error');
            //重试上传按钮
            $('#' + file.id + '_retry').show().find('.easyui-linkbutton').click(function () {
                opts.uploader.retry(file);
            });
        });

        opts.uploader.on('uploadComplete', function (file) {
        });
        //监听上传按钮
        $('#' + state.uploadBtnId).click(function () {
            opts.uploader.upload();
        });
    }

    //  格式化文件大小
    function renderSize(value) {
        if (null == value || value == '') {
            return "0 BT";
        }
        var unitArr = ["BT", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
        var srcsize = parseFloat(value);
        var index = Math.floor(Math.log(srcsize) / Math.log(1024));
        var size = srcsize / Math.pow(1024, index);
        //  保留的小数位数
        size = size.toFixed(2);
        return size + unitArr[index];
    }

    function setItem(target, file) {
        file.state = file.state ? file.state : '等待上传'
        var fileSize = renderSize(file.size);
        var state = $.data(target, "webuploader"), opts = state.options;
        var item = $('<div class="webuploader-item"/>');
        // swf: $.util.getRootPath() + '/rsframe/plugins/jeasyui/extend/jquery-easyui-webuploader/webuploader/Uploader.swf';
        var hasExt = 'pdf,doc,docx,xls,xlsx,ppt,pptx,pdf,txt,zip';
        var fileExt = 'file';
        if ($.string.contains(hasExt, file.ext)) {
            fileExt = file.ext;
        }
        var svgPath = $.util.getRootPath() + '/rsframe/plugins/jeasyui/extend/jquery-easyui-webuploader/img/' + fileExt + '.svg';
        var pngPath = $.util.getRootPath() + '/rsframe/plugins/jeasyui/extend/jquery-easyui-webuploader/img/' + fileExt + '.png';
        var filePath = file.savePath ? $.util.getRootPath() + file.savePath : "#";
        // 创建缩略图
        // 如果为非图片文件，可以不用调用此方法。
        // thumbnailWidth x thumbnailHeight 为 100 x 100
        if ($.string.contains(file.mimeType, 'image/')) {
            svgPath = $.util.getRootPath() + file.savePath;
        } else {
            opts.uploader.makeThumb(file, function (error, src) {
                if (!error) {
                    $('#' + file.id + '_thumb').attr('src', src);
                }
            }, 50, 50);
        }
        item.append('<img id="' + file.id + '_thumb" src="' + svgPath + '" onerror=this.src="' + pngPath + '" height="50" width="50" />');
        var filename = $('<div class="filename"/>');
        filename.append('<div><b><a id="' + file.id + '_href" href="' + filePath + '" target="_blank">' + file.name + '</a></b></div>');
        filename.append('<div id="' + file.id + '_progressbar" class="easyui-progressbar" style="width: auto;"></div>');
        item.append(filename);
        item.append('<div><div>' + fileSize + '</div><div id="' + file.id + '_state">' + file.state + '</div></div>');
        var handler = $('<div/>');
        handler.append('<div id="' + file.id + '_retry" style="display: none;"><a class="easyui-linkbutton" data-options="iconCls:\'icon-add\'">重试</a></div>');
        var remove = $('<div><a class="easyui-linkbutton" data-options="iconCls:\'icon-cancel\'">删除</a></div>');
        handler.append(remove);
        item.append(handler);
        var body = $('#' + state.webuploaderId).find('.body');
        body.prepend(item);
        $.parser.parse('.webuploader-box');
        $('#' + file.id + '_progressbar').progressbar('setValue', file.progress ? file.progress : 0);
        remove.find('.easyui-linkbutton').click(function () {
            for (var i = 0; i < opts.completefiles.length; i++) {
                if (file.id == opts.completefiles[i].id) {
                    opts.completefiles.splice(i, 1);
                    break;
                }
            }
            item.remove();
            if (opts.uploader.getFile(file.id)) {
                opts.uploader.removeFile(file.id, true);
            }
            setHideInputValue(target);
        });
        setHideInputValue(target);
    }

    function setHideInputValue(target) {
        var state = $.data(target, "webuploader"), opts = state.options;
        if (opts.completefiles.length > 0) {
            $(target).val($.util.parseJSONStr(opts.completefiles));
        } else {
            $(target).val("");
        }
    }

    function setValue(target, file) {
        var state = $.data(target, "webuploader"), opts = state.options;
        if ($.util.isString(file)) {
            file = JSON.parse(file);
        }
        if ($.util.isArray(file)) {
            setValues(target, file);
        } else {
            opts.completefiles.push(file);
            setHideInputValue(target);
            setItem(target, file);
        }
    }

    function setValues(target, files) {
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            file.name = file.fullName;
            file.id = $.util.guid("N");
            file.state = '加载完成';
            file.size = file.attSize;
            file.progress = 100.00;
            file.type = file.mimeType;
            setValue(target, file);
        }
    }

    function getOptions(target) {
        var state = $.data(target, "webuploader");
        if (state) {
            return state.options;
        }
        return null;
    };

    $.fn.webuploader = function (options, param) {
        if (typeof options == "string") {
            var method = $.fn.webuploader.methods[options];
            if (method) {
                return method(this, param);
            } else {
                return this.webuploader("panel").panel(options, param);
            }
        }
        options = options || {};
        return this.each(function () {
            var state = $.data(this, "webuploader");
            if (state) {
                $.extend(state.options, options);
            } else {
                $.data(this, "webuploader", {options: $.extend({}, $.fn.webuploader.defaults, $.fn.webuploader.parseOptions(this), options)});
                create(this);
            }
        });
    };


    function operateQueue(mini, method) {
        var btn = $(mini), t = btn.closest("div.webuploader-wrapper").find(".webuploader-f"),
            state = $.data(t[0], "webuploader"), opts = state.options;
        if (!opts.disabled && !state.wrapper.is(".webuploader-disabled")) {
            var index = window.parseInt(btn.closest("tr.datagrid-row").attr("datagrid-row-index")),
                row = state.grid.datagrid("getRowData", index);
            if (row.filestatus == -4) {
                var msg = {
                    cancel: opts.message.fileUploadSuccessCanntRemove,
                    upload: opts.message.fileUploadSuccess
                };
                $.easyui.messager.show(msg[method]);
            } else {
                state.webuploader.webuploader(method, row.id);
            }
        }
        if (window.event && window.event.stopPropagation) {
            window.event.stopPropagation();
        }
        return false;
    }

    $.fn.webuploader.cancelQueue = function (mini) {
        return operateQueue(mini, "cancel");
    };

    $.fn.webuploader.uploadQueue = function (mini) {
        return operateQueue(mini, "upload");
    };


    $.fn.webuploader.parseOptions = function (target) {
        return $.extend({}, $.parser.parseOptions(target, [
            "buttonClass", "buttonCursor", "buttonImage", "buttonText", "checkExisting", "fileObjName",
            "fileTypeDesc", "fileTypeExts", "itemTemplate", "method", "progressData", "queueID", "swf", "uploader",
            "uploadText", "stopText", "cancelText", "removeText", "emptyText", "finishText", "errorText",
            "buttonIcon", "stopIcon", "cancelIcon", "uploadIcon", "multiTemplate",
            "value", "width", "height", "missingMessage", "tipPosition", "separator",
            {
                auto: "boolean",
                debug: "boolean",
                multi: "boolean",
                preventCaching: "boolean",
                removeCompleted: "boolean",
                requeueErrors: "boolean",
                disabled: "boolean",
                buttonPlain: "boolean",
                showStop: "boolean",
                showCancel: "boolean",
                required: "boolean",
                novalidate: "boolean"
            },
            {
                fileSizeLimit: "number",
                height: "number",
                queueSizeLimit: "number",
                removeTimeout: "number",
                successTimeout: "number",
                uploadLimit: "number",
                width: "number",
                buttonWidth: "number",
                buttonHeight: "number",
                deltaX: "number"
            }
        ]));
    };


    //  提供外部调用公共方法 $.easyui.showwebuploaderDialog 用于快速创建 webuploader 文件上传对话框；
    //  该方法的参数 options 参考继承于方法 $.easyui.showDialog 和插件 easyui-webuploader 的参数格式，在在此基础上增加了如下描述的属性：
    //      title   : string
    //      value   : string
    //      onEnter : function(val, webuploader, webuploader, btn)
    //      onCancel: function(val, webuploader, webuploader, btn)
    //      ...
    //  返回值：返回创建的 easyui-dialog 对象；
    $.easyui.showwebuploaderDialog = function (options) {
        if (options && options.topMost && $ != $.util.$ && $.util.$.easyui.showwebuploaderDialog) {
            return $.util.$.easyui.showwebuploaderDialog.apply(this, options);
        }
        var opts = $.extend({}, $.easyui.showwebuploaderDialog.defaults, options || {}),
            webuploader, webuploader, value = opts.value,
            dia = $.easyui.showDialog($.extend({}, opts, {
                content: "<input class=\"webuploader-dialog-container\" />",
                enableApplyButton: false, topMost: false,
                height: opts.height == "auto" ? (opts.multi ? 480 : 160) : opts.height,
                onBeforeDestroy: function () {
                    if (webuploader) {
                        webuploader.webuploader("destroy");
                    }
                },
                onSave: function () {
                    return opts.onEnter.call(dia, getValue(), webuploader, webuploader, this);
                },
                onClose: function () {
                    return opts.onCancel.call(dia, getValue(), webuploader, webuploader, this);
                }
            }));
        $.util.exec(function () {
            var container = dia.find(".webuploader-dialog-container").webuploader($.extend({}, opts, {
                fit: true, border: false, noheader: true, value: value
            }));
            dia.webuploader = webuploader = container;
            webuploader = container.webuploader("webuploader");
            webuploader.webuploader("resize");
        });

        function getValue() {
            if (!webuploader || !webuploader) {
                return value;
            }
            var eopts = webuploader.webuploader("options");
            return webuploader.webuploader(eopts.multi ? "getValues" : "getValue");
        }

        return dia;
    };

    $.easyui.showwebuploaderDialog.defaults = {
        title: "文件上传对话框",
        iconCls: "icon-hamburg-publish",
        width: 750, height: "auto", minWidth: 400, minHeight: 160,
        maximizable: true, collapsible: true,
        multi: false, value: null,
        onEnter: function (val, editor, btn) {
        },
        onCancel: function (val, editor, btn) {
        }
    };


    $.fn.webuploader.methods = {

        options: function (jq) {
            return getOptions(jq[0]);
        },

        panel: function (jq) {
            return $.data(jq[0], "webuploader").panel;
        },

        webuploader: function (jq) {
            return getwebuploader(jq[0]);
        },

        buttons: function (jq) {
            return getButtons(jq[0]);
        },

        queueDOM: function (jq) {
            return getQueueDOM(jq[0]);
        },

        grid: function (jq) {
            return getGrid(jq[0]);
        },

        //  从当前上传队列中取消一个或多个文件的上传；该方法的参数 param 可以定义为如下数据类型：
        //      String  : 表示要取消上传的文件的 id 值；如果不定义该参数，则取消队列中第一个文件的上传；如果该值定义为 "*"，则取消队列中所有文件上传；
        //      Boolean : 默认为 false；如果定义为 true，则执行该方法时将不触发 onUploadCancel 事件；
        //  返回值：返回表示当前 easyui-ewebuploader 控件的 jQuery 链式对象。
        cancel: function (jq, param) {
            return jq.each(function () {
                cancel(this, param);
            });
        },

        //  从当前文档上下文中销毁该 easyui-ewebuploader 控件；该方法将触发 onDestroy 事件；
        //  返回值：返回表示当前 easyui-ewebuploader 控件的 jQuery 链式对象。
        destroy: function (jq) {
            return jq.each(function () {
                destroy(this);
            });
        },

        //  禁用该 easyui-ewebuploader 控件；该方法将触发 onDisable 事件；
        //      该方法的参数 setDisabled 为 Boolean 类型值，为 true 时表示禁用上传按钮；为 false 表示启用上传按钮；
        //  注意：该方法会导致当前的上传任务停止；
        //  返回值：返回表示当前 easyui-ewebuploader 控件的 jQuery 链式对象。
        disable: function (jq, setDisabled) {
            return jq.each(function () {
                disable(this, setDisabled);
            });
        },

        //  获取或设置 easyui-ewebuploader 控件中指定名称的属性值；该方法的参数 param 可以定义为如下数据类型：
        //      String  : 该参数指定一个属性名称，该属性名所示的属性值将会被返回；
        //      Object  : 格式如 { name: string, value: object }；设定指定名称的属性为指定的值。
        //  返回值：返回表示当前 easyui-ewebuploader 控件中指定名称的属性值；或者返回表示当前 easyui-ewebuploader 控件的 jQuery 链式对象。
        settings: function (jq, param) {
            return settings(jq[0], param);
        },

        //  停止当前上传队列中所有文件的上传。
        //  返回值：返回表示当前 easyui-ewebuploader 控件的 jQuery 链式对象。
        stop: function (jq) {
            return jq.each(function () {
                stop(this);
            });
        },

        //  获取当前上传队列中有多少文件数量；
        //  返回值：返回一个 array 类型数组，数组中的每一项都是一个 file 对象；
        queues: function (jq) {
            return getQueues(jq[0]);
        },

        //  立即上传当前队列中的特定文件或所有文件。该方法的参数 param 可以定义为如下数据类型：
        //      String  : 表示要立即上传的文件 id 值；如果不定义该参数，则上传队列中的第一个文件；如果该值定义为 "*"，则上传队列中所有文件；
        //  返回值：返回表示当前 easyui-ewebuploader 控件的 jQuery 链式对象。
        upload: function (jq, param) {
            return jq.each(function () {
                upload(this, param);
            });
        },

        setFormData: function (jq, param) {
            return jq.each(function () {
                setFormData(this, param);
            });
        },

        resetFormData: function (jq) {
            return jq.each(function () {
                resetFormData(this);
            });
        },

        clearFormData: function (jq) {
            return jq.each(function () {
                clearFormData(this);
            });
        },

        resize: function (jq, param) {
            return jq.each(function () {
                resize(this, param);
            });
        },

        enable: function (jq) {
            return jq.each(function () {
                enable(this);
            });
        },


        validate: function (jq) {
            return jq.each(function () {
                validate(this);
            });
        },

        isValid: function (jq) {
            return validate(jq[0]);
        },

        enableValidation: function (jq) {
            return jq.each(function () {
                setValidation(this, false)
            });
        },

        disableValidation: function (jq) {
            return jq.each(function () {
                setValidation(this, true)
            });
        },

        clear: function (jq) {
            return jq.each(function () {
                clear(this);
            });
        },

        reset: function (jq) {
            return jq.each(function () {
                reset(this);
            });
        },

        getValues: function (jq) {
            return getValues(jq[0]);
        },

        setValues: function (jq, values) {
            return jq.each(function () {
                setValues(this, values);
            });
        },

        getValue: function (jq) {
            return getValue(jq[0]);
        },

        setValue: function (jq, value) {
            return jq.each(function () {
                setValue(this, value);
            });
        },


        show: function (jq) {
            return jq.each(function () {
                show(this);
            });
        },

        hide: function (jq) {
            return jq.each(function () {
                hide(this);
            });
        }
    };

    $.fn.webuploader.defaults = {
        //  表示当选择了待上传文件后，是否自动执行 upload 方法以上传文件；
        auto: true,


        //  当上传队列中的一个或多个文件被执行 cancel 方法而从队列中取消时，该事件将会被触发；
        //      file    : object 类型值；表示被取消上传的文件对象；
        onCancel: function (file) {
        },

        //  当 cancel 方法被执行并且参数值为 "*"，该事件将会被触发；
        //      queueItemCount: number 类型值；表示取消上传的文件总数量
        onClearQueue: function () {
        },

        //  当 destroy 方法被调用时，该事件将会被触发；
        onDestroy: function () {
        },

        //  当浏览文件对话框被关闭时，该事件将会被触发；如果该事件会添加至 overrideEvents 属性，则在将文件添加到上传队列中时如果出现错误，将不会弹出警告消息；
        //      queueData: object 类型值，格式如 { filesSelected: number, filesQueued: number, filesReplaced: number, filesCancelled: number, filesErrored: number }
        onDialogClose: function (queueData) {
        },

        //  当打开浏览文件对话框后，该事件将会被触发；注意，该事件函数不会在浏览文件对话框打开时被立即执行，而是在窗口关闭时执行。
        onDialogOpen: function () {
        },

        //  当 disable 方法被调用时，该事件将会被触发；
        onDisable: function () {
        },

        //  当 disable 方法被调用并且 setDisabled 值为 false 以启用按钮时，该事件将会被触发；
        onEnable: function () {
        },

        //  在上传控件初始化过程中，如果当前浏览器的 Flash 插件版本不兼容，该事件将会被触发；
        onFallback: function () {
        },

        //  在 easyui-ewebuploader 控件第一次被初始化完成后，该事件将会被触发；
        //      instance:   表示 webuploader 对象；
        onInit: function (instance) {
        },

        //  当上传队列中所有文件被处理完成后，该事件将会被触发；
        //      queueData:  object 类型值，格式如 { uploadsSuccessful: number, uploadsErrored: number }
        onQueueComplete: function (queueData) {
        },

        //  当打开文件浏览器对话框并选择了要上传的文件之后，该事件将会被触发；
        //      file:   表示被选择的待上传文件；
        onSelect: function (file) {
        },

        //  当打开文件浏览器对话框并在选择文件出错后，该事件将会被触发；针对每个选择文件出错后，该事件都会被触发一次；
        //      file:   表示触发异常事件的文件对象；
        //      errorCode:表示错误代码，可能的值为如下几种：
        //          QUEUE_LIMIT_EXCEEDED:   选定的文件数量超过限制；
        //          FILE_EXCEEDS_SIZE_LIMIT:选定的文件大小超过限制；
        //          INVALID_FILETYPE:       选定的文件类型超过限制；
        //      errorMsg:表示触发该事件时的错误消息；
        onSelectError: function (file, errorCode, errorMsg) {
        },

        //  当文件上传完成(不管成功还是失败)，该事件将会被触发；针对每个上传的文件，该事件都会触发一次；
        //      file:   表示触发该事件的文件对象；
        onUploadComplete: function (file) {
        },

        //  当文件上传失败后，该事件将会被触发；针对每个上传失败的文件，该事件都会触发一次；
        //      file:   表示触发该事件的文件对象；
        //      errorCode:  表示错误编号
        //      errorMsg:   表示错误消息
        //      errorString:表示错误消息字符串，可能包含所有的错误细节的可读内容；
        onUploadError: function (file, errorCode, errorMsg, errorString) {
        },

        //  当文件上传进度每次更新时，该事件将会被触发；针对每个上传文件的每次进度更新，该事件都会触发一次；
        //      file:   表示触发该事件的文件对象；
        //      bytesUploaded: 表示该文件自开始上传时至当前时刻已上传的字节总数；
        //      bytesTotal:    表示该文件的字节总数；
        //      totalBytesUploaded: 表示目前为止所有文件的上传字节总数；
        //      totalBytesTotal: 表示所有文件的字节总数
        onUploadProgress: function (file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
        },

        //  当文件上传进度每次更新时，该事件将会被触发；
        //      file:   表示将要被上传的文件对象；
        onUploadStart: function (file) {
        },

        //  当文件上传成功完成时，该事件将会被触发；针对每个上传成功的文件，该事件都会触发一次；
        //      file:   表示上传完成的文件对象；
        //      data:   文件上传完成后，由服务器端返回的数据；
        //      response: 表示文件是否上传成功；boolean 类型值；如果为 true 则表示文件上传成功；
        //          如果该值为 false，则表示当提交上传后并超过 successTimeout 属性设置的超时时间后，服务器任未有数据返回(文件上传失败)；
        onUploadSuccess: function (file, data, response) {
        }
    };


    $.extend($.fn.webuploader.defaults, {
        successTimeout: 3600,
        removeCompleted: false
    });


    $.parser.plugins.push("webuploader");

    if ($.fn.form && $.isArray($.fn.form.otherList)) {
        $.fn.form.otherList.push("webuploader");
    }

})(jQuery);