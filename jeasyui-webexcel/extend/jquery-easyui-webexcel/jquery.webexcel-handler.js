(function ($) {

    $.fn.webexcel.plus.handler = function (target) {

        var state = $.data(target, "webexcel"), opts = state.options;

        //画布列右键菜单操作
        opts.colMenuHandler = function (item, e) {
            if (item.name == 'insertCol') {//插入列
                opts.addWholeCol(1, 1);
            } else if (item.name == 'addCol') {//添加列
                opts.addWholeCol(2, 1);
            } else if (item.name == 'removeCol') {//删除列
                opts.removeWholeCol();
            }
        };

        //画布行右键菜单操作
        opts.rowMenuHandler = function (item, e) {
            if (item.name == 'insertRow') {//插入行
                opts.addWholeRow(1, 1);
            } else if (item.name == 'addRow') {//添加行
                opts.addWholeRow(2, 1);
            } else if (item.name == 'removeRow') {//删除行
                opts.removeWholeRow();
            }
        };

        //画布内容区右键菜单操作
        opts.contentMenuHandler = function (item, e) {
            if (item.name == 'mergeCell' && opts.chooseCellOption) {
                if (opts.chooseCellOption.hasMergeCell) {
                    //如果有合并单元格，拆分单元格
                    opts.splitChooseCell(target);//拆分单元格
                } else {
                    opts.mergeChooseCell(target);//合并单元格
                }
            } else if (item.name == 'textBold') {
                //字体加粗
                opts.chooseFirstCell.bold = !opts.chooseFirstCell.bold;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'hAlignLeft') {
                //水平居左
                opts.chooseFirstCell.hAlign = 1;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'hAlignCenter') {
                //水平居中
                opts.chooseFirstCell.hAlign = 2;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'hAlignRight') {
                //水平居右
                opts.chooseFirstCell.hAlign = 3;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'vAlignLeft') {
                //垂直居上
                opts.chooseFirstCell.vAlign = 1;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'vAlignCenter') {
                //垂直居中
                opts.chooseFirstCell.vAlign = 2;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'vAlignRight') {
                //垂直居下
                opts.chooseFirstCell.vAlign = 3;
                opts.drawCanvasCell(opts.contentContext, opts.chooseFirstCell);
            } else if (item.name == 'exportExcel') {
                //导出excel
                opts.doExportExcel();
            } else if (item.name == 'noBorder') {
                //无框线
                opts.setBorderSize(1);
            } else if (item.name == 'allBorder') {
                //所有框线
                opts.setBorderSize(2);
            } else if (item.name == 'outBorder') {
                //外框线
                opts.setBorderSize(3);
            } else if (item.name == 'topBorder') {
                //上框线
                opts.setBorderSize(4);
            } else if (item.name == 'bottomBorder') {
                //下框线
                opts.setBorderSize(5);
            } else if (item.name == 'leftBorder') {
                //左框线
                opts.setBorderSize(6);
            } else if (item.name == 'rightBorder') {
                //右框线
                opts.setBorderSize(7);
            } else if (item.name == 'topBottomBorder') {
                //上下框线
                opts.setBorderSize(8);
            } else if (item.name == 'leftRightBorder') {
                //左右框线
                opts.setBorderSize(9);
            } else if (item.name == 'bgColor') {
                //背景颜色
                var c = ColorPicker(opts.colorPickerDiv[0],
                    function (hex, hsv, rgb) {
                        for (var key in opts.chooseCellOption.cells) {
                            var cell = opts.chooseCellOption.cells[key];
                            cell.bgColor = hex;
                            opts.drawCanvasCell(opts.contentContext, cell);
                        }
                    }
                );
                c.setHex(opts.chooseFirstCell.bgColor);
                var x = Math.ceil((opts.container.width() - opts.colorPickerDiv.width()) / 2) + opts.scrollDot.x;
                var y = Math.ceil((opts.container.height() - opts.colorPickerDiv.height()) / 2) + opts.scrollDot.y;
                opts.colorPickerDiv.css({left: x, top: y}).show();
            }
        };
    };

})(jQuery);