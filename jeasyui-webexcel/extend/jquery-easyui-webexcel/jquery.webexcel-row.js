(function ($) {
    $.fn.webexcel.plus.row = function (target) {
        var state = $.data(target, "webexcel"), opts = state.options;
        //设置表格行高
        opts.settingIndexRowResize = function () {
            opts.tableResizeBox.hide();
            var resizeHeight = opts.tableResizeBox.height() - opts.indexRowResizeCell.h;
            var beginRow = opts.indexRowResizeCell.beginRow;
            var borderSize = opts.chooseBorderSize * 2;

            //遍历索引行数据，更改单元格宽度，并画单元格
            var key, cell;
            for (key in opts.data.indexColData) {
                cell = opts.data.indexColData[key];
                if (cell.beginRow == beginRow) {
                    cell.h = opts.tableResizeBox.height() + borderSize;
                } else if (cell.beginRow > beginRow) {
                    cell.y = cell.y + resizeHeight + borderSize;
                }
            }
            //遍历表格数据，更改单元格宽度，并画单元格
            for (key in opts.data.contentData) {
                cell = opts.data.contentData[key];
                if (cell.beginRow == beginRow) {
                    cell.h = opts.tableResizeBox.height() + borderSize;
                } else if (cell.beginRow > beginRow) {
                    cell.y = cell.y + resizeHeight + borderSize;
                }
            }
            opts.doLoadData(target, opts.data);//画单元格
            //重新画选择框
            if (opts.chooseBoxCss) {
                opts.mouseDown = {
                    x: opts.chooseBoxCss.left + opts.cellDefault.borderSize,
                    y: opts.chooseBoxCss.top + opts.cellDefault.borderSize
                };
                opts.mouseMove = {
                    x: opts.mouseDown.x + opts.chooseBoxCss.width - opts.cellDefault.borderSize * 2,
                    y: opts.mouseDown.y + opts.chooseBoxCss.height + resizeHeight - opts.cellDefault.borderSize * 2
                };
                opts.drawChooseBox(target);
            }
        };
        //显示索引行高设置动画
        opts.showIndexRowResize = function (target, e, isDown) {
            var cell = opts.indexRowResizeCell;
            var cellDot = {
                x: cell.x - Math.ceil(opts.cellDefault.borderSize / 2),
                y: cell.y + opts.contentCanvasDot.y - Math.ceil(opts.cellDefault.borderSize / 2)
            };
            var width = cell.w + opts.contentCanvas[0].width - opts.cellDefault.borderSize;
            var height = cell.h;
            if (!isDown) {
                var dot = {x: e.clientX, y: e.clientY};
                dot = opts.dotCalculate(dot, opts.containerDot, 0);
                dot = opts.dotCalculate(dot, opts.scrollDot, 1);
                height = dot.y - cellDot.y - opts.cellDefault.borderSize;
            }
            opts.tableResizeBox.css({
                left: 0,
                top: cellDot.y,
                height: height,
                width: width,
                border: opts.cellDefault.borderSize + 'px dotted ' + opts.resizeBorderColor,
                'border-left': 0,
                'border-right': 0
            }).show();
        };
        //选择的整行单元格
        opts.chooseWholeRow = function (target, dot) {
            opts.chooseWholeRowData = null;

            for (var colKey in opts.data.indexColData) {
                var colData = opts.data.indexColData[colKey];
                var borderSize = opts.cellDefault.borderSize * 2;
                if (dot.y >= colData.y + borderSize && dot.y <= colData.y + colData.h - borderSize) {
                    opts.chooseWholeRowData = colData;
                }
            }
            if (opts.chooseWholeRowData) {
                dot = {x: opts.chooseWholeRowData.x, y: opts.chooseWholeRowData.y};
                dot = opts.dotCalculate(dot, opts.contentCanvasDot, 1);
                var size = Math.ceil(opts.cellDefault.borderSize / 2);//解决小数像素偏差问题
                opts.chooseBoxCss = {
                    left: dot.x - size,
                    top: dot.y - size,
                    width: opts.indexRowCanvas.width() - opts.chooseBorderSize * 2 - size * 2,
                    height: opts.chooseWholeRowData.h - opts.chooseBorderSize * 2 + size * 2,
                    border: opts.chooseBorderSize + 'px solid ' + opts.chooseBorderColor
                };
                if (opts.cellDefault.borderSize == 1) {
                    opts.chooseBoxCss.width += size * 2;
                }
                opts.chooseBox.css(opts.chooseBoxCss).show();
            }
        };
        //删除整行
        opts.removeWholeRow = function (total) {
            total = total ? total : 1;
            //计算被删除的宽度
            var totalHeight = 0;
            for (var i = 0; i < total; i++) {
                var removeId = 'indexCol_r' + (opts.chooseWholeRowData.beginRow + i) + '_c0';
                var removeCell = opts.data.indexColData[removeId];
                if (removeCell) {
                    totalHeight = totalHeight + removeCell.h;
                }
            }
            //遍历索引列数据，更改单元格ID，并删除需删除的行数据
            var indexColData = {};
            var key, cell, newCell;
            for (key in opts.data.indexColData) {
                cell = opts.data.indexColData[key];
                if (cell.beginRow < opts.chooseWholeRowData.beginRow) {
                    indexColData[cell.id] = cell;
                } else if (cell.beginRow >= opts.chooseWholeRowData.beginRow + total) {
                    for (var i = 1; i <= total; i++) {
                        newCell = $.extend(true, {}, cell);
                        newCell.beginRow = cell.beginRow - total;
                        newCell.endRow = cell.endRow - total;
                        newCell.id = 'indexCol_r' + newCell.beginRow + '_c0';
                        newCell.text = newCell.beginRow + 1;
                        newCell.y = newCell.y - totalHeight;
                        indexColData[newCell.id] = newCell;
                    }
                } else {
                    //被删除的单元格,舍弃
                }
            }
            opts.data.indexColData = indexColData;
            //遍历表格数据，更改单元格ID，并删除需删除的行数据
            var data = {}, mergeCells = {};
            for (key in opts.data.contentData) {
                cell = opts.data.contentData[key];
                if (cell.beginRow < opts.chooseWholeRowData.beginRow) {
                    data[cell.id] = cell;
                } else if (cell.beginRow >= opts.chooseWholeRowData.beginRow + total) {
                    for (var i = 1; i <= total; i++) {
                        newCell = $.extend(true, {}, cell);
                        newCell.beginRow = cell.beginRow - total;
                        newCell.endRow = cell.endRow - total;
                        newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                        newCell.y = newCell.y - totalHeight;
                        data[newCell.id] = newCell;
                    }
                } else {
                    //被删除的单元格,舍弃
                    //记录合并单元格
                    if (cell.merged) {
                        mergeCells[cell.id] = $.extend(true, {}, cell);
                    }
                    if (cell.mergeId) {
                        var mergeCell = opts.data.contentData[cell.mergeId];
                        mergeCells[mergeCell.id] = $.extend(true, {}, mergeCell);
                    }
                }
            }
            //处理合并单元格
            for (var key in mergeCells) {
                var cell = mergeCells[key];
                if (cell.endRow - (cell.beginRow + total) >= 0) {//如果还够删
                    cell.endRow = cell.endRow - total;
                    cell.h = cell.h - totalHeight;
                    data[cell.id] = cell;
                }
            }
            opts.data.contentData = data;
            var chooseRow = opts.data.indexColData[opts.chooseWholeRowData.id];
            if (!chooseRow) {//如果是最后一行
                chooseRow = opts.data.indexColData['indexCol_r' + (opts.chooseWholeRowData.beginRow - 1) + '_c0']
            }
            var borderSize = opts.cellDefault.borderSize * 2;
            opts.doLoadData(target, opts.data);//画单元格
            opts.chooseWholeRow(target, {x: chooseRow.x + borderSize, y: chooseRow.y + borderSize});
        };
        /*
        插入整行、增加整行
        status：1.插入整行；2.增加整行
        total：插入行数、增加行数
        */
        opts.addWholeRow = function (status, total) {
            total = total ? total : 1;
            var referRow = opts.chooseWholeRowData;//插入参照行,默认选择行
            if (status == 2 || !referRow) {
                //添加行在最后一行后添加
                referRow = opts.data.indexColData['indexCol_r' + (opts.rowCount - 1) + '_c0'];
            }

            //处理数据
            var indexColData = {}, data = {};
            if (status == 2) {
                var key, cell, newCell;
                for (key in opts.data.indexColData) {
                    cell = opts.data.indexColData[key];
                    indexColData[cell.id] = cell;
                }
                for (key in opts.data.contentData) {
                    cell = opts.data.contentData[key];
                    data[cell.id] = cell;
                }
                for (var i = 1; i <= total; i++) {
                    //添加索引列
                    var newCell = $.extend(true, {}, referRow);
                    newCell.beginRow = referRow.beginRow + i;
                    newCell.endRow = referRow.endRow + i;
                    newCell.y = newCell.y + opts.rowHeight * i;
                    newCell.text = newCell.beginRow + 1;
                    newCell.id = 'indexCol_r' + newCell.beginRow + '_c0';
                    indexColData[newCell.id] = newCell;
                    // 添加内容行
                    for (key in opts.data.indexRowData) {
                        var rowCell = opts.data.indexRowData[key];
                        var newCell = $.extend(true, {}, opts.cellDefault);
                        newCell.beginRow = referRow.beginRow + i;
                        newCell.endRow = referRow.endRow + i;
                        newCell.beginCol = rowCell.beginCol;
                        newCell.endCol = rowCell.endCol;
                        newCell.borderColor = opts.contentBorderColor;
                        newCell.x = rowCell.x;
                        newCell.y = referRow.y + opts.rowHeight * i;
                        newCell.w = rowCell.w;
                        newCell.h = referRow.h;
                        newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                        newCell.text = '';
                        data[newCell.id] = newCell;
                    }
                }
            } else {
                //遍历索引列数据，更改单元格ID，并插入需插入的行数据
                var key, cell, newCell, mergeCells = {};
                for (key in opts.data.indexColData) {
                    cell = opts.data.indexColData[key];
                    if (cell.beginRow < referRow.beginRow) {
                        //小于选择列的索引单元格不用动
                        indexColData[cell.id] = cell;
                    } else {
                        if (cell.beginRow == referRow.beginRow) {
                            //插入新索引单元格
                            for (var i = 0; i < total; i++) {
                                newCell = $.extend(true, {}, cell);
                                newCell.beginRow = cell.beginRow + i;
                                newCell.endRow = cell.endRow + i;
                                newCell.y = newCell.y + opts.rowHeight * i;
                                newCell.text = newCell.beginRow + 1;
                                newCell.id = 'indexCol_r' + newCell.beginRow + '_c0';
                                indexColData[newCell.id] = newCell;
                            }
                        }
                        //调整原索引单元格信息
                        newCell = $.extend(true, {}, cell);
                        newCell.beginRow = cell.beginRow + total;
                        newCell.endRow = cell.endRow + total;
                        newCell.y = newCell.y + opts.rowHeight * total;
                        newCell.text = newCell.beginRow + 1;
                        newCell.id = 'indexCol_r' + newCell.beginRow + '_c0';
                        indexColData[newCell.id] = newCell;
                    }
                }
                //遍历表格数据，更改单元格ID，并插入需插入的行数据
                for (key in opts.data.contentData) {
                    cell = opts.data.contentData[key];
                    if (cell.beginRow < referRow.beginRow) {
                        data[cell.id] = cell;
                    } else {
                        if (cell.beginRow == referRow.beginRow) {
                            //插入新数据
                            for (var i = 0; i < total; i++) {
                                newCell = $.extend(true, {}, cell);
                                newCell.y = cell.y + opts.rowHeight * i;
                                newCell.beginRow = cell.beginRow + i;
                                newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                                newCell.text = '';
                                //重置因合并影响的单元格
                                newCell.endRow = cell.beginRow + i;
                                newCell.endCol = cell.beginCol + i;
                                newCell.w = opts.colWidth;
                                newCell.h = opts.rowHeight;
                                newCell.showCell = true;
                                newCell.merged = false;
                                newCell.mergeId = null;
                                data[newCell.id] = newCell;
                            }
                            if (cell.mergeId) {
                                mergeCells[cell.mergeId] = opts.data.contentData[cell.mergeId];
                            }
                        }
                        newCell = $.extend(true, {}, cell);
                        newCell.beginRow = cell.beginRow + total;
                        newCell.endRow = cell.endRow + total;
                        newCell.y = newCell.y + opts.rowHeight * total;
                        newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                        data[newCell.id] = newCell;
                    }
                }
                //处理合并单元格
                for (var key in mergeCells) {
                    var cell = mergeCells[key];
                    if (cell.beginRow < referRow.beginRow) {//如果在合并单元格中插入
                        cell.endRow = cell.endRow + total;
                        cell.h = cell.h + opts.rowHeight * total;
                        data[cell.id] = cell;
                    }
                }
            }
            opts.data.indexColData = indexColData;//重设索引数据
            opts.data.contentData = data;// 重设数据
            opts.doLoadData(target, opts.data);//画单元格
        };
    };

})(jQuery);