(function ($) {
    $.fn.webexcel.plus.col = function (target) {

        var state = $.data(target, "webexcel"), opts = state.options;

        //设置表格列宽
        opts.settingIndexColResize = function () {
            opts.tableResizeBox.hide();
            var resizeWidth = opts.tableResizeBox.width() - opts.indexColResizeCell.w;
            var beginCol = opts.indexColResizeCell.beginCol;
            var borderSize = opts.chooseBorderSize * 2;

            //遍历索引行数据，更改单元格宽度
            var key, cell;
            for (key in opts.data.indexRowData) {
                cell = opts.data.indexRowData[key];
                if (cell.beginCol == beginCol) {
                    cell.w = opts.tableResizeBox.width() + borderSize;
                } else if (cell.beginCol > beginCol) {
                    cell.x = cell.x + resizeWidth + borderSize;
                }
            }
            //遍历表格数据，更改单元格宽度
            for (key in opts.data.contentData) {
                cell = opts.data.contentData[key];
                if (cell.beginCol == beginCol) {
                    cell.w = opts.tableResizeBox.width() + borderSize;
                } else if (cell.beginCol > beginCol) {
                    cell.x = cell.x + resizeWidth + borderSize;
                }
            }
            opts.doLoadData(target, opts.data);//画单元格
            //重新画选择框
            if (opts.chooseBoxCss) {
                opts.mouseDown = {
                    x: opts.chooseBoxCss.left + opts.cellDefault.borderSize,
                    y: opts.chooseBoxCss.top + opts.cellDefault.borderSize
                };
                opts.mouseMove = {
                    x: opts.mouseDown.x + opts.chooseBoxCss.width + resizeWidth - opts.cellDefault.borderSize * 2,
                    y: opts.mouseDown.y + opts.chooseBoxCss.height - opts.cellDefault.borderSize * 2
                };
                opts.drawChooseBox(target);
            }
        };

        //显示索引列宽设置动画
        opts.showIndexColResize = function (target, e, isDown) {
            var cell = opts.indexColResizeCell;
            var cellDot = {
                x: cell.x + opts.contentCanvasDot.x - Math.ceil(opts.cellDefault.borderSize / 2),
                y: cell.y - Math.ceil(opts.cellDefault.borderSize / 2)
            };
            var height = cell.h + opts.contentCanvas[0].height - opts.cellDefault.borderSize;
            var width = cell.w - opts.cellDefault.borderSize;
            if (!isDown) {
                var dot = {x: e.clientX, y: e.clientY};
                dot = opts.dotCalculate(dot, opts.containerDot, 0);
                dot = opts.dotCalculate(dot, opts.scrollDot, 1);
                width = dot.x - cellDot.x - opts.cellDefault.borderSize;
            }
            opts.tableResizeBox.css({
                left: cellDot.x,
                top: 0,
                height: height,
                width: width,
                border: opts.cellDefault.borderSize + 'px dotted ' + opts.resizeBorderColor,
                'border-top': 0,
                'border-bottom': 0
            }).show();
        };

        //选择的整列单元格
        opts.chooseWholeCol = function (target, dot) {
            opts.chooseWholeColData = null;

            for (var rowKey in opts.data.indexRowData) {
                var rowData = opts.data.indexRowData[rowKey];
                var borderSize = opts.cellDefault.borderSize * 2;
                if (dot.x >= rowData.x + borderSize && dot.x <= rowData.x + rowData.w - borderSize) {
                    opts.chooseWholeColData = rowData;
                }
            }
            if (opts.chooseWholeColData) {
                dot = {x: opts.chooseWholeColData.x, y: opts.chooseWholeColData.y};
                dot = opts.dotCalculate(dot, opts.contentCanvasDot, 1);
                var size = Math.ceil(opts.cellDefault.borderSize / 2);//解决小数像素偏差问题
                opts.chooseBoxCss = {
                    left: dot.x - size,
                    top: dot.y - size,
                    width: opts.chooseWholeColData.w - opts.chooseBorderSize * 2 + size * 2,
                    height: opts.indexColCanvas.height() - opts.chooseBorderSize * 2 - size * 2,
                    border: opts.chooseBorderSize + 'px solid ' + opts.chooseBorderColor
                };
                if (opts.cellDefault.borderSize == 1) {
                    opts.chooseBoxCss.height += size * 2;
                }
                opts.chooseBox.css(opts.chooseBoxCss).show();
            }
        };

        //删除整列
        opts.removeWholeCol = function (total) {
            total = total ? total : 1;
            //计算被删除的宽度
            var totalWidth = 0;
            for (var i = 0; i < total; i++) {
                var removeId = 'indexRow_r0_c' + (opts.chooseWholeColData.beginCol + i);
                var removeCell = opts.data.indexRowData[removeId];
                if (removeCell) {
                    totalWidth = totalWidth + removeCell.w;
                }
            }
            //遍历索引行数据，更改单元格ID，并删除需删除的列数据
            var indexRowData = {};
            var key, cell, newCell;
            for (key in opts.data.indexRowData) {
                cell = opts.data.indexRowData[key];
                if (cell.beginCol < opts.chooseWholeColData.beginCol) {
                    indexRowData[cell.id] = cell;
                } else if (cell.beginCol >= opts.chooseWholeColData.beginCol + total) {
                    for (var i = 1; i <= total; i++) {
                        newCell = $.extend(true, {}, cell);
                        newCell.beginCol = cell.beginCol - total;
                        newCell.endCol = cell.endCol - total;
                        newCell.id = 'indexRow_r0_c' + newCell.beginCol;
                        newCell.text = opts.indexToColumn(newCell.beginCol);
                        newCell.x = newCell.x - totalWidth;
                        indexRowData[newCell.id] = newCell;
                    }
                } else {
                    //被删除的单元格,舍弃
                }
            }
            opts.data.indexRowData = indexRowData;
            //遍历表格数据，更改单元格ID，并删除需删除的列数据
            var data = {}, mergeCells = {};
            for (key in opts.data.contentData) {
                cell = opts.data.contentData[key];
                if (cell.beginCol < opts.chooseWholeColData.beginCol) {
                    data[cell.id] = cell;
                } else if (cell.beginCol >= opts.chooseWholeColData.beginCol + total) {
                    for (var i = 1; i <= total; i++) {
                        newCell = $.extend(true, {}, cell);
                        newCell.beginCol = cell.beginCol - total;
                        newCell.endCol = cell.endCol - total;
                        newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                        newCell.x = newCell.x - totalWidth;
                        data[newCell.id] = newCell;
                    }
                } else {
                    //被删除的单元格,舍弃
                    //记录合并单元格
                    if (cell.merged) {
                        mergeCells[cell.id] = $.extend(true, {}, cell);
                    }
                    if (cell.mergeId) {
                        var mergeCell = opts.data.contentData[cell.mergeId];
                        mergeCells[mergeCell.id] = $.extend(true, {}, mergeCell);
                    }
                }
            }
            //处理合并单元格
            for (var key in mergeCells) {
                var cell = mergeCells[key];
                if (cell.endCol - (cell.beginCol + total) >= 0) {//如果还够删
                    cell.endCol = cell.endCol - total;
                    cell.w = cell.w - totalWidth;
                    data[cell.id] = cell;
                } else {
                    //否则,说明合并单元格已经被删了
                }
            }
            opts.data.contentData = data;
            var chooseCol = opts.data.indexRowData[opts.chooseWholeColData.id];
            if (!chooseCol) {//如果是最后一列
                chooseCol = opts.data.indexRowData['indexRow_r0_c' + (opts.chooseWholeColData.beginCol - 1)]
            }
            var borderSize = opts.cellDefault.borderSize * 2;
            opts.doLoadData(target, opts.data);//画单元格
            opts.chooseWholeCol(target, {x: chooseCol.x + borderSize, y: chooseCol.y + borderSize});
        };

        /*
        插入整列、增加整列
        status：1.插入整列；2.增加整列
        total：插入列数、增加列数
        */
        opts.addWholeCol = function (status, total) {
            total = total ? total : 1;
            var referCol = opts.chooseWholeColData;//插入参照列,默认选择列
            if (status == 2 || !referCol) {
                //添加列在最后一行后添加
                referCol = opts.data.indexRowData['indexRow_r0_c' + (opts.colCount - 1)];
            }

            //处理数据
            var indexRowData = {}, data = {};
            if (status == 2) {
                var key, cell, newCell;
                for (key in opts.data.indexRowData) {
                    cell = opts.data.indexRowData[key];
                    indexRowData[cell.id] = cell;
                }
                for (key in opts.data.contentData) {
                    cell = opts.data.contentData[key];
                    data[cell.id] = cell;
                }
                for (var i = 1; i <= total; i++) {
                    //添加索引行
                    var newCell = $.extend(true, {}, referCol);
                    newCell.beginCol = referCol.beginCol + i;
                    newCell.endCol = referCol.endCol + i;
                    newCell.x = newCell.x + opts.colWidth * i;
                    newCell.text = opts.indexToColumn(newCell.beginCol);
                    newCell.id = 'indexRow_r0_c' + newCell.beginCol;
                    indexRowData[newCell.id] = newCell;
                    // 添加内容行
                    for (key in opts.data.indexColData) {
                        var colCell = opts.data.indexColData[key];
                        var newCell = $.extend(true, {}, opts.cellDefault);
                        newCell.beginRow = colCell.beginRow;
                        newCell.endRow = colCell.endRow;
                        newCell.beginCol = referCol.beginCol + i;
                        newCell.endCol = referCol.endCol + i;
                        newCell.borderColor = opts.contentBorderColor;
                        newCell.x = referCol.x + opts.colWidth * i;
                        newCell.y = colCell.y;
                        newCell.w = referCol.w;
                        newCell.h = colCell.h;
                        newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                        newCell.text = '';
                        data[newCell.id] = newCell;
                    }
                }
            } else {
                //遍历索引行数据，更改单元格ID，并插入需插入的列数据
                var key, cell, newCell;
                for (key in opts.data.indexRowData) {
                    cell = opts.data.indexRowData[key];
                    if (cell.beginCol < referCol.beginCol) {
                        //小于选择行的索引单元格不用动
                        indexRowData[cell.id] = cell;
                    } else {
                        if (cell.beginCol == referCol.beginCol) {
                            //插入新索引单元格
                            for (var i = 0; i < total; i++) {
                                newCell = $.extend(true, {}, cell);
                                newCell.beginCol = cell.beginCol + i;
                                newCell.endCol = cell.endCol + i;
                                newCell.x = newCell.x + opts.colWidth * i;
                                newCell.text = opts.indexToColumn(newCell.beginCol);
                                newCell.id = 'indexRow_r0_c' + newCell.beginCol;
                                indexRowData[newCell.id] = newCell;
                            }
                        }
                        //调整原索引单元格信息
                        newCell = $.extend(true, {}, cell);
                        newCell.beginCol = cell.beginCol + total;
                        newCell.endCol = cell.endCol + total;
                        newCell.x = newCell.x + opts.colWidth * total;
                        newCell.id = 'indexRow_r0_c' + newCell.beginCol;
                        newCell.text = opts.indexToColumn(newCell.beginCol);
                        indexRowData[newCell.id] = newCell;
                    }
                }
                //遍历表格数据，更改单元格ID，并插入需插入的列数据
                var mergeCells = {};
                for (key in opts.data.contentData) {
                    cell = opts.data.contentData[key];
                    if (cell.beginCol < referCol.beginCol) {
                        data[cell.id] = cell;
                    } else {
                        if (cell.beginCol == referCol.beginCol) {
                            //插入新数据
                            for (var i = 0; i < total; i++) {
                                newCell = $.extend(true, {}, cell);
                                newCell.x = cell.x + opts.colWidth * i;
                                newCell.beginCol = cell.beginCol + i;
                                newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                                newCell.text = '';
                                //重置因合并影响的单元格
                                newCell.endRow = cell.beginRow + i;
                                newCell.endCol = cell.beginCol + i;
                                newCell.w = opts.colWidth;
                                newCell.h = opts.rowHeight;
                                newCell.showCell = true;
                                newCell.merged = false;
                                newCell.mergeId = null;
                                data[newCell.id] = newCell;
                            }
                            if (cell.mergeId) {
                                mergeCells[cell.mergeId] = opts.data.contentData[cell.mergeId];
                            }
                        }
                        newCell = $.extend(true, {}, cell);
                        newCell.beginCol = cell.beginCol + total;
                        newCell.endCol = cell.endCol + total;
                        newCell.x = newCell.x + opts.colWidth * total;
                        newCell.id = 'content_r' + newCell.beginRow + '_c' + newCell.beginCol;
                        data[newCell.id] = newCell;
                    }
                }
                //处理合并单元格
                for (var key in mergeCells) {
                    var cell = mergeCells[key];
                    if (cell.beginCol < referCol.beginCol) {//如果在合并单元格中插入
                        cell.endCol = cell.endCol + total;
                        cell.w = cell.w + opts.colWidth * total;
                        data[cell.id] = cell;
                    }
                }
            }
            opts.data.indexRowData = indexRowData;//重设索引数据
            opts.data.contentData = data;// 重设数据
            opts.doLoadData(target, opts.data);//画单元格
        };
    };

})
(jQuery);