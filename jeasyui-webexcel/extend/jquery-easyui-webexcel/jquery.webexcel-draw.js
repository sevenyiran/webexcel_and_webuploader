(function ($) {

    $.fn.webexcel.plus.draw = function (target) {
        var state = $.data(target, "webexcel"), opts = state.options;

        //加载数据，独立封装，禁止其他地方画webExcel，方便统一调整相关参数,以及健壮性
        opts.doLoadData = function (target, data) {
            if (!data) {
                opts.width = opts.height = 0;
                for (var i = 0; i < opts.colCount; i++) {
                    addIndexRowData(i);
                    opts.width += opts.colWidth;
                }
                for (var i = 0; i < opts.rowCount; i++) {
                    addIndexColData(i);
                    opts.height += opts.rowHeight;
                }
                initContentData();
            } else {
                opts.data = $.extend(true, opts.data, data);

                //根据数据初始化索引列和索引行
                for (var key in opts.data.contentData) {
                    var cell = opts.data.contentData[key];
                    var indexColCell = opts.data.indexColData['indexCol_r' + cell.beginRow + '_c0'];
                    var indexRowCell = opts.data.indexRowData['indexRow_r0_c' + cell.beginCol];
                    //如果没有索引列数据,添加索引数据
                    if (!indexColCell) {
                        addIndexColData(cell.beginRow);
                    }
                    //如果没有索引行数据,添加索引数据
                    if (!indexRowCell) {
                        addIndexRowData(cell.beginCol);
                    }
                }
                //初始化基础属性,及调整坐标
                opts.width = opts.height = opts.rowCount = opts.colCount = 0;
                for (var key in opts.data.indexRowData) {
                    var cell = opts.data.indexRowData[key];
                    opts.colCount++;
                    opts.width += cell.w;
                }
                for (var key in opts.data.indexColData) {
                    var cell = opts.data.indexColData[key];
                    opts.rowCount++;
                    opts.height += cell.h;
                }
                //初始化索引基础属性,调整坐标
                var initFlag = true, mergeCells = {};
                for (var colIndex = 0; colIndex < opts.rowCount; colIndex++) {
                    var indexColCell = opts.data.indexColData['indexCol_r' + colIndex + '_c0'];
                    var firstCell = opts.data.indexColData['indexCol_r' + (colIndex - 1) + '_c0'];
                    if (!indexColCell) {
                        break;
                    }
                    if (firstCell) {
                        indexColCell.y = firstCell.y + firstCell.h;
                    }
                    for (var rowIndex = 0; rowIndex < opts.colCount; rowIndex++) {
                        var indexRowCell = opts.data.indexRowData['indexRow_r0_c' + rowIndex];
                        if (!indexRowCell) {
                            break;
                        }
                        if (initFlag) {
                            var firstCell = opts.data.indexRowData['indexRow_r0_c' + (rowIndex - 1)];
                            if (firstCell) {
                                indexRowCell.x = firstCell.x + firstCell.w;
                            }
                        }
                        //处理数据
                        var id = 'content_r' + indexColCell.beginRow + '_c' + indexRowCell.beginCol;
                        var cell = opts.data.contentData[id];
                        if (!cell) {//如果没有单元格,则初始化单元格
                            cell = $.extend(true, {}, opts.cellDefault, {
                                id: id,
                                beginRow: indexColCell.beginRow,
                                beginCol: indexRowCell.beginCol,
                                endRow: indexColCell.endRow,
                                endCol: indexRowCell.endCol,
                                borderColor: opts.contentBorderColor,
                                x: indexRowCell.x,
                                y: indexColCell.y,
                                w: indexRowCell.w,
                                h: indexColCell.h
                            });
                        } else {
                            //设置默认值
                            cell = $.extend(true, {}, opts.cellDefault, cell);
                            //根据索引重设单元格的坐标,防止数据异常
                            if (indexRowCell && indexColCell && cell) {
                                cell.x = indexRowCell.x;
                                cell.y = indexColCell.y;
                                cell.w = indexRowCell.w;
                                cell.h = indexColCell.h;
                            }

                            var contentCell = opts.data.contentData[cell.id];
                            cell = $.extend(true, contentCell, cell);
                            if (cell.border) {
                                //使用默认边框颜色，边框颜色设置不提供给使用者
                                cell.borderColor = opts.setBorderColor;
                                cell.borderLeft = cell.borderTop = cell.borderRight = cell.borderBottom = cell.border;
                            }
                        }
                        //重置合并单元格占用信息
                        cell.showCell = true;
                        cell.mergeId = null;
                        if (cell.merged) {//如果是合并单元格记录
                            mergeCells[cell.id] = $.extend(true, {}, cell);
                        }
                        opts.data.contentData[cell.id] = cell;
                    }
                    initFlag = false;
                }
                //处理被合并单元格占用的单元格
                for (var key in mergeCells) {
                    var cell = mergeCells[key];
                    var w = 0, h = 0;
                    for (var i = cell.beginRow; i <= cell.endRow; i++) {
                        var indexColCell = opts.data.indexColData['indexCol_r' + i + '_c0'];
                        h += indexColCell.h;
                        for (var j = cell.beginCol; j <= cell.endCol; j++) {
                            if (i == cell.beginRow) {
                                var indexRowCell = opts.data.indexRowData['indexRow_r0_c' + j];
                                w += indexRowCell.w;
                            }
                            var id = 'content_r' + i + '_c' + j;
                            if (id != cell.id) {
                                //被合并单元格
                                var contentCell = opts.data.contentData[id];
                                contentCell.mergeId = cell.id;
                                contentCell.showCell = false;
                                opts.data.contentData[contentCell.id] = contentCell;
                            }
                        }
                    }
                    //重设合并单元格属性
                    cell.w = w;
                    cell.h = h;
                    cell.showCell = true;
                    opts.data.contentData[cell.id] = cell;
                }
            }
            //设置索引列宽度
            var context = opts.leftTopCanvas[0].getContext('2d');
            opts.indexColWidth = Math.ceil(context.measureText(opts.rowCount).width) + 15;
            for (var key in opts.data.indexColData) {
                var cell = opts.data.indexColData[key];
                //设置索引列单元格宽度
                cell.w = opts.indexColWidth;
            }
            drawWebExcel();
        };

        //添加顶部索引行
        function addIndexRowData(index) {
            var id = 'indexRow_r0_c' + index;
            return opts.data.indexRowData[id] = $.extend(true, {}, opts.cellDefault, {
                id: id,
                beginRow: 0,
                beginCol: index,
                endRow: 0,
                endCol: index,
                bgColor: opts.indexBgColor,
                borderColor: opts.indexBorderColor,
                x: opts.colWidth * index,
                y: 0,
                w: opts.colWidth,
                h: opts.rowHeight,
                bold: true,
                hAlign: 2,
                vAlign: 2,
                text: opts.indexToColumn(index)
            });
        }

        //初始化左侧索引列
        function addIndexColData(index) {
            var id = 'indexCol_r' + index + '_c0';
            return opts.data.indexColData[id] = $.extend(true, {}, opts.cellDefault, {
                id: id,
                beginRow: index,
                beginCol: 0,
                endRow: index,
                endCol: 0,
                bgColor: opts.indexBgColor,
                borderColor: opts.indexBorderColor,
                x: 0,
                y: opts.rowHeight * index,
                w: opts.indexColWidth,
                h: opts.rowHeight,
                bold: true,
                hAlign: 2,
                vAlign: 2,
                text: index + 1
            });
        }

        //初始化中间内容区
        function initContentData() {
            for (var i = 0; i < opts.rowCount; i++) {
                for (var j = 0; j < opts.colCount; j++) {
                    var id = 'content_r' + i + '_c' + j;
                    opts.data.contentData[id] = $.extend(true, {}, opts.cellDefault, {
                        id: id,
                        beginRow: i,
                        beginCol: j,
                        endRow: i,
                        endCol: j,
                        borderColor: opts.contentBorderColor,
                        x: opts.colWidth * j,
                        y: opts.rowHeight * i,
                        w: opts.colWidth,
                        h: opts.rowHeight
                    });
                }
            }
        }

        //画webExcel
        function drawWebExcel() {
            initCanvas();
            drawLeftTopCell();
            drawIndexRow();
            drawIndexCol();
            drawContent();
        }

        //画左上角固定单元格
        function drawLeftTopCell() {
            var cell = $.extend(true, {}, opts.cellDefault, {
                bgColor: opts.indexBgColor,
                borderColor: opts.indexBorderColor,
                x: 0,
                y: 0,
                w: opts.indexColWidth,
                h: opts.rowHeight
            });
            opts.drawCanvasCell(opts.leftTopContext, cell);
        }

        //画索引行
        function drawIndexRow() {
            for (var key in opts.data.indexRowData) {
                var cell = opts.data.indexRowData[key];
                opts.drawCanvasCell(opts.indexRowContext, cell);
            }
        }

        //画索引列
        function drawIndexCol() {
            for (var key in opts.data.indexColData) {
                var cell = opts.data.indexColData[key];
                opts.drawCanvasCell(opts.indexColContext, cell);
            }
        }

        //画索单元格编辑区
        function drawContent() {
            var defaultCells = [];//默认样式的单元格
            var hasBorderCells = [];//带边框的单元格
            for (var key in opts.data.contentData) {
                var cell = opts.data.contentData[key];
                if (cell.showCell) {
                    if (cell.border || cell.borderLeft || cell.borderTop || cell.borderRight || cell.borderBottom) {
                        //把有边框的单元格放到最后处理，避免边框被后一个单元格给覆盖掉
                        hasBorderCells.push(cell);
                    } else {
                        defaultCells.push(cell);
                    }
                }
            }
            //画单元格
            for (var i = 0; i < defaultCells.length; i++) {
                var defaultCell = defaultCells[i];
                opts.drawCanvasCell(opts.contentContext, defaultCell);
            }
            //有边框的单元格最后画，避免边框被后一个单元格给覆盖掉
            for (var i = 0; i < hasBorderCells.length; i++) {
                var borderCell = hasBorderCells[i];
                opts.drawCanvasCell(opts.contentContext, borderCell);
            }
        }

        //获取画布上下文，解决边线模糊问题
        function getCanvasContext(canvas) {
            // 拿到canvas绘图上下文
            var context = canvas.getContext('2d');
            var ratio = opts.getPixelRatio(context);
            canvas.style.width = canvas.width + 'px';
            canvas.style.height = canvas.height + 'px';
            canvas.width = canvas.width * ratio;
            canvas.height = canvas.height * ratio;
            // 放大倍数
            context.scale(ratio, ratio);
            return context;
        }

        //初始化画布
        function initCanvas() {
            //隐藏页面需重新初始化的控件
            opts.editInput.hide();
            opts.chooseBox.hide();
            opts.colorPickerDiv.hide();

            //设置容器坐标containerDot，为窗口的坐标
            var containerOffset = opts.container.offset();
            opts.containerDot = {x: containerOffset.left, y: containerOffset.top};

            //设置画布坐标contentCanvasDot，为相对于容器的坐标
            opts.contentCanvasDot = {x: opts.indexColWidth, y: opts.rowHeight};

            //设置宽高
            opts.container.css({
                width: document.body.clientWidth - opts.containerDot.x,
                height: document.body.clientHeight - opts.containerDot.y
            });

            var borderSize = opts.cellDefault.borderSize * 2;
            //设置左上角画布
            var w = opts.indexColWidth + borderSize;
            var h = opts.rowHeight + borderSize;
            var canvas = opts.leftTopCanvas[0];
            canvas.width = w;
            canvas.height = h;
            opts.leftTopContext = getCanvasContext(canvas);
            opts.leftTopCellDiv.css({
                left: opts.scrollDot.x,
                top: opts.scrollDot.y,
                width: w,
                height: h
            });

            //设置索引行画布
            var canvas = opts.indexRowCanvas[0];
            w = opts.width + borderSize;
            h = opts.rowHeight + borderSize;
            canvas.width = w;
            canvas.height = h;
            opts.indexRowContext = getCanvasContext(canvas);
            //根据索引列宽设置索引行位置
            opts.indexRowDiv.css({
                left: opts.indexColWidth,
                top: opts.scrollDot.y,
                width: w
            });

            //设置索引列画布
            var canvas = opts.indexColCanvas[0];
            w = opts.indexColWidth + borderSize;
            h = opts.height + borderSize;
            canvas.width = w;
            canvas.height = h;
            opts.indexColContext = getCanvasContext(canvas);
            //根据索引列宽设置索引列位置
            opts.indexColDiv.css({
                left: opts.scrollDot.x,
                top: opts.rowHeight,
                height: h
            });

            //设置内容区画布
            var canvas = opts.contentCanvas[0];
            w = opts.width + borderSize;
            h = opts.height + borderSize;
            canvas.width = w;
            canvas.height = h;
            opts.contentContext = getCanvasContext(canvas);
            //根据索引列宽设置索引列位置
            opts.contentDiv.css({
                left: opts.indexColWidth,
                top: opts.rowHeight,
                width: w,
                height: h
            });
        }

        //清空单元格
        opts.clearCanvasCell = function (context, cell) {
            context.clearRect(cell.x, cell.y, cell.w, cell.h);
        };

        //画单元格
        opts.drawCanvasCell = function (context, cell) {
            context.fillStyle = cell.bgColor;//背景颜色
            if (cell.bgColor == '#ffffffff') {//如果是透明背景色更改为白色，目的：兼容IE
                context.fillStyle = '#ffffff';//白色背景颜色
            }
            context.fillRect(cell.x, cell.y, cell.w, cell.h);//设置背景
            context.lineWidth = cell.borderSize;//设置单元格边框
            context.strokeStyle = cell.borderColor;//单元格边框颜色
            context.fillStyle = cell.textColor;//设置文字颜色
            context.font = cell.fontSize + 'px ' + cell.font;//单元格字体
            context.strokeRect(cell.x, cell.y, cell.w, cell.h);
            opts.drawCanvasCellText(context, cell);
        };

        //画单元格中的文字
        opts.drawCanvasCellText = function (context, cell) {
            var textList = opts.handlerCellText(context, cell);
            var maxIndex = cell.h / cell.lineHeight;//单元格能容纳的内容高度
            if (textList.length < maxIndex) {
                maxIndex = textList.length;
            }
            var yOffset = Math.ceil((cell.h - maxIndex * cell.lineHeight) / 2);

            for (var i = 0; i < maxIndex; i++) {
                var text = textList[i];
                var textW = context.measureText(text).width;
                //文本X水平居中坐标=单元格X坐标-文字的宽度/2+单元格的宽度/2+边线厚度
                var textX = Math.ceil(cell.x - textW / 2 + cell.w / 2 + context.lineWidth - cell.borderSize);
                //文本Y垂直居中坐标=单元格Y坐标-字体的大小/4+单元格的高度/2+边线厚度
                var textY = Math.ceil(cell.y + cell.fontSize / 4 + cell.h / 2 + context.lineWidth - cell.borderSize);
                if (cell.hAlign == 1) {//水平居左
                    textX = cell.x + cell.padding;
                } else if (cell.hAlign == 3) {//水平居右
                    textX = cell.x + cell.w - textW - cell.padding;
                }
                //处理文字行高
                textY = textY - Math.ceil(cell.lineHeight * maxIndex / 2 - cell.lineHeight / 2);
                if (cell.vAlign == 1) {//垂直居上
                    textY = textY - yOffset;
                } else if (cell.vAlign == 3) {//垂直居下
                    textY = textY + yOffset;
                }
                //处理多行偏移量
                textY = textY + cell.lineHeight * i;
                if (cell.bold) {
                    context.fillText(text, textX - 0.5, textY);//字体加粗
                    context.fillText(text, textX, textY - 0.5);//字体加粗
                } else {
                    context.fillText(text, textX, textY);
                }
            }
        };
    };

})(jQuery);